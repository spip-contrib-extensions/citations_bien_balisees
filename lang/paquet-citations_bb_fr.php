<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-citations_bb
// Langue: fr
// Date: 18-07-2019 13:58:58
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'citations_bb_description' => 'Ce plugin transforme automatiquement le raccourci {&lt;quote&gt;} en {&lt;q&gt;} (et {&lt;/quote&gt;} en {&lt;/q&gt;}) si la citation ne contient pas de retour à la ligne.

	L\'objectif est de respecter les usages en HTML (les citations courtes doivent être entourées par &lt;q&gt;, les citations contenant des paragraphes doivent être entourées des &lt;blockquote&gt;).',
	'citations_bb_slogan' => 'Traitement typographique des citations courtes',
);
?>